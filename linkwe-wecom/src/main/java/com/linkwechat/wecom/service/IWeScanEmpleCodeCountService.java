package com.linkwechat.wecom.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.linkwechat.wecom.domain.WeScanEmpleCodeCount;

public interface IWeScanEmpleCodeCountService extends IService<WeScanEmpleCodeCount> {
}
